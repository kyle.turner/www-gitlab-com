---
layout: handbook-page-toc
title: Unverified Email Requests
category: GitLab.com
subcategory: Accounts
description: Process for removing an unverified email from a user account for GitLab.com.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

See the [Releasing an Email Address](https://about.gitlab.com/handbook/support/workflows/account_changes.html#releasing-an-email-address) workflow for details about releasing an unverified email from a user account on GitLab.com.
